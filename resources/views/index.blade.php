<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>LET'S CONSULT</title>

    <meta name="description" content="Let's Consult, vypracovanie zadania.">
    <meta name="author" content="Radoslav Kečka">

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

</head>
<body>
    <div id="content">
        <h1>{{ __('Server Rack') }}</h1>
        <div class="row">
            <table>
                <thead>
                    <tr>
                        <th>Pozícia</th>
                        <th>Obsadená</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($positions as $position)
                        <tr>
                            <th>{{ $position->id }}</th>
                            <th>
                                <input type="checkbox" name="position-id" class="position-checkbox" value="1" {{$position->occupied != 1 ?: 'checked'}} data-id="{{$position->id}}" data-occupied="{{$position->occupied == 1 ? 0 : 1}}">
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div>
                <form action="{{ url('/') }}" method="post">
                    @csrf
                    <label for="device_size"> Veľkosť zariadenia
                        <input type="number" name="device_size" value="{{ !isset($device_size)?: $device_size}}">
                    </label>
                    <button type="submit">Odoslať</button>
                </form>
            </div>
            <div>
                @if(isset($valid_positions))
                    <div>
                        <p><span class="red">ID pozicii</span> kde je mozne zariadenie o velkosti {{$device_size}} umiesnit:</p>
                        @foreach($valid_positions as $valid_position)
                            {{ $valid_position }}
                        @endforeach
                    </div>

                @endif
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
    <script>
        $('.position-checkbox').on('change', function(e){
            updatePosition(this);
        });

        function updatePosition(evnt){
            var id = $(evnt).attr('data-id');
            var occupied = $(evnt).attr('data-occupied');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ url('position-update') }}",
                data: {
                    id : id,
                    occupied : occupied,
                },
            });
        }
    </script>

</body>
</html>
