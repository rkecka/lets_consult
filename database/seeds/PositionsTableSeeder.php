<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = [
            ['occupied' => 0],
            ['occupied' => 1],
            ['occupied' => 1],
            ['occupied' => 0],
            ['occupied' => 0],
            ['occupied' => 0],
            ['occupied' => 0],
            ['occupied' => 0],
        ];

        DB::table('positions')->insert($positions);
    }
}
