<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use tests\Mockery\Adapter\Phpunit\EmptyTestCase;

class Position extends Model
{
    protected $fillable = ['occupied'];

    public function getPositions(){
        return $this->get();
    }

    public function checkValidPositions($size){
        $positions = $this->get();

        $i = 0;
        $j = 0;
        $valid_positions = [];
        foreach($positions as $position){
            if($position->occupied == 0 ){
                $i++;
                if($i >= $size){
                    $j++;
                    $valid_positions[] .= ($position->id - $size + 1);
                }
            } else {
                $i = 0;
            }
        }
        if($j === 0) {
            $valid_positions[] .= 'Nenašla sa vhodná pozícia';
        }

        return $valid_positions;
    }
}
