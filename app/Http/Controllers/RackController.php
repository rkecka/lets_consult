<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;

class RackController extends Controller
{
    protected $position;

    public function __construct()
    {
        $this->position = new Position();
    }

    public function index(){
        $positions = $this->position->getPositions();

        return view('index')->with(['positions' => $positions]);
    }

    public function validPositions(Request $request){

        $valid_positions = $this->position->checkValidPositions($request->device_size);
        $positions = $this->position->getPositions();

        return view('index')->with(['positions' => $positions,'device_size' => $request->device_size, 'valid_positions' => $valid_positions]);
    }

    public function positionUpdate(Request $request){
        $position = Position::find($request->id);
        $position->occupied = $request->occupied;
        $position->save();
    }
}
